# antichrist

A frontend for elixire using Next.js and TypeScript.

## Development

The following tools are used:

- [Node.js] and [npm]
- [TypeScript]
- [Prettier]
- [eslint]

Please make sure you are using these tools appropriately as you work on this
project.

[typescript]: https://www.typescriptlang.org
[eslint]: https://eslint.org
[prettier]: https://prettier.io
[node.js]: https://nodejs.org
[npm]: https://www.npmjs.com

## Deployment

Deployment requires [Node.js] and [npm].

[npm]: https://www.npmjs.com

### Configuration

Copy the example configuration and edit it:

```sh
$ cp config.example.json config.json
$ nano config.json
```

Example configuration (with comments):

```js
{
  // The name of the instance. This will appear in the header, so keep it short.
  "instanceName": "elixi.re",

  // The domain to assign the token cookie to. This should be the domain that
  // the frontend will be accessible from. It MUST be accessed through HTTPS and
  // HTTPS only. (The cookie is specified with the "secure" option.)
  //
  // In a development environment, this value is ignored, and the cookie will
  // always be created with a compatible domain.
  "cookieDomain": "elixi.re",

  // The external base URL of your elixire instance.
  // This is used to make API requests on the client.
  "externalUrl": "https://elixi.re",

  // The internal base URL of your elixire instance.
  // This is used to make API requests on the server during server-side rendering.
  "internalUrl": "http://localhost:9988",

  // Forward the `X-Forwarded-For` header to the backend. Set this to `true` if
  // antichrist will be serving requests behind a reverse proxy such as nginx.
  "respectXForwardedFor": false
}
```

## Building

Install dependencies:

```sh
$ npm ci
```

Build the website:

```sh
$ npm run build
```

This will output the JavaScript bundle with optimizations to `.next`.

## Serving

Serve the website:

```sh
$ npm run start -- -p 4000 -H 0.0.0.0
```

This will spawn an HTTP server on `0.0.0.0:4000`. Use a reverse proxy such as
[nginx] to forward external traffic to this server. Keep in mind that both the
client and server may make requests to the elixire app server (each with their
own API URL as configured in `config.json`).

[nginx]: https://www.nginx.com

## In loving memory of

This project is dedicated to Eli Murray.

February 25th, 1999 - August 28th, 2019

---

This repository's icon uses [Mutant Standard emoji](https://mutant.tech), which
are licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).
