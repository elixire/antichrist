import React from "react";

export type NavigatorState = {
  userAgent: string | null;
};

// The navigator context is used to provide information about the navigator
// (the client's browser) throughout the app. This is used to personalize
// keyboard shortcuts and whatnot.
export const NavigatorContext = React.createContext<NavigatorState>({
  userAgent: null,
});
