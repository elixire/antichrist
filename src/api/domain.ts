export type Domain = {
  domain: string;
  id: number;
  permissions: number;
  tags: string[];
};
