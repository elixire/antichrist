export type Profile = {
  id: string;
  name: string;
  email: string;

  active: boolean;
  admin: boolean;
  consented: boolean | null;
  dump_status: object | null;
  paranoid: boolean;

  subdomain: string;
  domain: number;
  shorten_domain: number | null;
  shorten_subdomain: string;

  limits: {
    file_byte_limit: number;
    file_byte_used: number;
    shorten_limit: number;
    shorten_used: number;
  };

  stats: {
    total_bytes: number;
    total_deleted_files: number;
    total_files: number;
    total_shortens: number;
  };
};
