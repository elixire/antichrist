export interface APIErrorResponse {
  error: true;
  message: string;
}
