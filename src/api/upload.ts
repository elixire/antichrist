import NanoEvents from "nanoevents";
import nanoid from "nanoid";
import { serverUrl, computeRequestHeaders } from "../request";
import { APIErrorResponse } from ".";

export enum UploadState {
  /** The upload hasn't started yet. */
  INACTIVE,

  /** The file is uploading. */
  UPLOADING,

  /** The upload has succeeded. */
  SUCCEEDED,

  /** The upload has errored. */
  ERRORED,

  /** The upload was aborted. */
  ABORTED,
}

export interface UploadResponse {
  url: string;
  shortname: string;
}

export interface Upload {
  /** The `UploadRequest` abstraction associated with this upload. */
  request: UploadRequest;

  /** The successful response of the upload request. Is `null` when an error happens. */
  response: UploadResponse | null;

  /** The error response of the upload request. Is `null` when there's no error. */
  error: APIErrorResponse | null;

  /** A number from 0 to 1 indicating the progress of the upload request. */
  progress: number;

  /** The current state of the upload. */
  state: UploadState;
}

type UploadRequestEvents = {
  start: () => void;
  succeed: (response: object) => void;
  progress: (percent: number) => void;
  abort: () => void;
  error: (response: object | null) => void;
};

export class UploadRequest extends NanoEvents<UploadRequestEvents> {
  public id: string;
  public file: File;
  public formData: FormData;
  public request: XMLHttpRequest | null = null;

  public constructor(file: File) {
    super();
    this.id = nanoid();
    this.file = file;
    this.formData = new FormData();
    this.formData.append("file", file, file.name);
  }

  public send({ asAdmin }: { asAdmin: boolean }): XMLHttpRequest {
    let endpoint = "/api/upload";
    if (asAdmin) {
      endpoint += "?admin=1";
    }

    const self = this;

    // we are using XMLHttpRequest here instead of `fetch` or our custom wrapper
    // around it, `request`, because both do not support streaming request
    // progress (yet).

    const request = new XMLHttpRequest();
    request.responseType = "json";

    request.upload.addEventListener("loadstart", () => this.emit("start"));
    request.upload.addEventListener("progress", (event: ProgressEvent) => {
      this.emit("progress", event.loaded / event.total);
    });
    request.upload.addEventListener("abort", () => this.emit("abort"));

    request.addEventListener("load", function () {
      const ok = this.status >= 200 && this.status < 300;
      if (ok) {
        self.emit("succeed", this.response);
      } else {
        self.emit("error", this.response);
      }
    });
    request.addEventListener("error", () => this.emit("error", null));

    request.open("POST", serverUrl() + endpoint);

    request.setRequestHeader("Accept", "application/json");
    const headers = computeRequestHeaders(document.cookie);
    for (const [header, value] of Object.entries(headers)) {
      request.setRequestHeader(header, value);
    }

    request.send(this.formData);

    this.request = request;
    return request;
  }
}
