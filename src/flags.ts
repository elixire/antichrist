const FLAG_PREFIX = "\u{1f3f3}\u{fe0f}\u{200d}";

/** trans rights */
export const TRANS_FLAG = FLAG_PREFIX + "\u{26a7}\u{fe0f}";

/** gay rights */
export const GAY_FLAG = FLAG_PREFIX + "\u{1f308}";
