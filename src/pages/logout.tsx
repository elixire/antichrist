import React from "react";
import Router from "next/router";

import { AuthContext } from "../auth";
import { AuthState } from "../auth/state";
import log from "../log";
import { deleteTokenCookie } from "../auth/cookie";

export default class Logout extends React.Component {
  public static contextType = AuthContext;

  private logout(): void {
    const authState: AuthState = this.context;

    if (!authState.authed) {
      Router.replace("/");
      return;
    }

    deleteTokenCookie();

    // Refresh the auth state, causing this component to render again and kick
    // us back to /.
    log("logging out, refreshing auth state");
    authState.actions!.refreshAuthState();
  }

  public componentDidMount(): void {
    // logout upon revisiting
    this.logout();
  }

  public componentDidUpdate(): void {
    this.logout();
  }

  public render(): React.ReactElement {
    return <p>Logging out...</p>;
  }
}
