import React from "react";

import { instanceName } from "../../config.json";
import { AuthContext } from "../auth";
import Uploader from "../components/Uploader";

function renderGuest(): React.ReactElement {
  return (
    <>
      <p>
        Welcome to {instanceName}, a free and open source image host and link
        shortener.
      </p>
    </>
  );
}

function renderMember(): React.ReactElement {
  return (
    <>
      <Uploader />
    </>
  );
}

const Index: React.FC = (): React.ReactElement => {
  const authState = React.useContext(AuthContext);

  return authState.authed ? renderMember() : renderGuest();
};

export default Index;
