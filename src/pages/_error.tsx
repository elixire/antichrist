import React from "react";
import Link from "next/link";
import { GetServerSideProps } from "next";

const errorMessages: Record<number, React.ReactNode> = {
  404: (
    <p>
      That page couldn't be found.{" "}
      <Link href="/">
        <a>Go home?</a>
      </Link>
    </p>
  ),
  500: <p>Something went wrong. Try again?</p>,
};

type Props = {
  statusCode: number;
};

export default function Error({ statusCode }: Props): React.ReactElement {
  const message = errorMessages[statusCode] || <p>Oops!</p>;

  return (
    <>
      <h1>{statusCode}</h1>
      {message}
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async ctx => {
  return { props: { statusCode: ctx.res.statusCode } };
};
