import React from "react";
import App, { AppInitialProps, AppContext, AppProps } from "next/app";
import "@fortawesome/fontawesome-svg-core/styles.css";
import "@reach/listbox/styles.css";
import { version as ANTICHRIST_VERSION } from "../../package.json";

import { NavigatorState, NavigatorContext } from "../context";
import { AuthState, computeAuthState, AuthStateActions } from "../auth/state";
import { AuthContext } from "../auth";
import log from "../log";
import Layout from "../components/Layout";

import "../styles/global.css";
import { getTokenFromCookies } from "../auth/cookie";
import { NextPageContext } from "next";
import { TRANS_FLAG, GAY_FLAG } from "../flags";

type State = {
  authState: AuthState;
  navigatorState: NavigatorState | null;
};

export default class AntichristApp extends App<{}, {}, State> {
  public constructor(props: AppProps) {
    super(props);

    log(
      `\u{1f985} this is antichrist ${ANTICHRIST_VERSION} ` +
        `${TRANS_FLAG} ${GAY_FLAG}`
    );

    const { pageProps } = this.props;

    this.state = {
      navigatorState: { userAgent: pageProps.userAgent },
      authState: {
        ...pageProps.partialAuthState,
        actions: this.authStateActions,
      },
    };
  }

  private static async computePageProps(ctx: NextPageContext): Promise<any> {
    let pageProps: Record<string, any> = {};

    // Inject the user agent. This is easily accessible clientside, but we need
    // it during server rendering.
    pageProps.userAgent = ctx.req!.headers["user-agent"];

    try {
      if (ctx.req!.headers?.cookie != null) {
        const authState = await computeAuthState(null, {
          serverContext: ctx,
          quiet: false,
        });
        let partialAuthState: Record<string, any> = authState;
        delete partialAuthState.actions;
        pageProps.partialAuthState = partialAuthState;
      }
    } catch (error) {
      // It's OK if we fail to fetch the user's profile.
    } finally {
      pageProps.partialAuthState = pageProps.partialAuthState ?? {
        authed: false,
      };
    }

    return pageProps;
  }

  public static async getInitialProps({
    Component,
    ctx,
  }: AppContext): Promise<AppInitialProps> {
    let pageProps: Record<string, any> = {};

    // If we're server rendering, inject some important data into `pageProps`.
    // This includes a partial auth state and the browser user agent.
    //
    // The partial auth state allows us to have an auth state during server
    // rendering and later hydrate without any hiccups.
    if (ctx.req != null) {
      pageProps = await this.computePageProps(ctx);
    }

    if (Component.getInitialProps) {
      Object.assign(pageProps, await Component.getInitialProps(ctx));
    }

    return { pageProps };
  }

  private get authStateActions(): AuthStateActions {
    return {
      refreshAuthState: async () => {
        const authState = await this.computeAuthState();
        this.setState({ authState }, () => {
          log("refreshed auth state: ", this.state.authState);
        });
      },
    };
  }

  /**
   * Recalculates the auth state based on the token and profile.
   *
   * This is really only called on the client, because the auth state is only
   * calculated once: during server rendering.
   */
  public computeAuthState = async (): Promise<AuthState> => {
    const {
      pageProps: { userProfile },
    } = this.props;

    const actions = this.authStateActions;
    const token = getTokenFromCookies(document.cookie);

    if (token == null) {
      // The token mysteriously vanished?
      return { authed: false, actions };
    }

    if (userProfile != null) {
      // Just use the user profile that the server renderer gave us.
      return { authed: true, actions, profile: userProfile, token };
    } else {
      // Or fetch it if we really need to...like when we're logging in!
      return computeAuthState(actions, { serverContext: null, quiet: false });
    }
  };

  public render(): React.ReactElement {
    const { Component, pageProps } = this.props;

    return (
      <NavigatorContext.Provider value={this.state.navigatorState!}>
        <AuthContext.Provider value={this.state.authState}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </AuthContext.Provider>
      </NavigatorContext.Provider>
    );
  }
}
