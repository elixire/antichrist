import React from "react";
import { Formik, FormikHelpers } from "formik";
import Link from "next/link";

import SignupForm, {
  SignupFormValues,
  schema,
  initialValues,
} from "../forms/signup";
import Notice from "../components/Notice";
import Hue from "../hue";
import request from "../request";

const Signup: React.FC = () => {
  const [registrationState, setRegistrationState] = React.useState<
    "none" | "activated" | "pending"
  >("none");

  async function handleSubmit(
    values: SignupFormValues,
    actions: FormikHelpers<SignupFormValues>
  ) {
    try {
      const resp = await request<{
        user_id: number;
        require_approvals: boolean;
      }>("/api/auth/register", {
        method: "POST",
        json: {
          // eslint-disable-next-line
          discord_user: values.discordTag,
          name: values.username,
          password: values.password,
          email: values.email,
        },
      });

      setRegistrationState(resp.require_approvals ? "pending" : "activated");
    } catch (error) {
      actions.setStatus(`Failed to signup: ${error.message}`);
      return;
    } finally {
      actions.setSubmitting(false);
    }
  }

  let contents;
  switch (registrationState) {
    case "none":
      contents = (
        <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          component={SignupForm}
          validationSchema={schema}
        />
      );
      break;
    case "pending":
      contents = (
        <Notice hue={Hue.Green}>
          <p>
            <strong>Account requested. Check your email!</strong>
          </p>
          <p>Please keep the orange notice above in mind.</p>
        </Notice>
      );
      break;
    case "activated":
      contents = (
        <Notice hue={Hue.Green}>
          <p>
            <strong>Account registered!</strong> You can{" "}
            <Link href="/login">
              <a>log in</a>
            </Link>{" "}
            now.
          </p>
        </Notice>
      );
      break;
  }

  return (
    <>
      <h1>Signup</h1>
      <Notice hue={Hue.Orange}>
        <p>
          <strong>The service is currently invite-only.</strong>
        </p>
        <p>
          An account begins its review after its owner joins the Discord server.
          However, in order to join the Discord server, you must be referred by
          someone who already has an account.
        </p>
        <p>
          Currently, joining the Discord server means an automatic kick unless
          you have been whitelisted by a developer. Make sure your friend
          mentions your Discord user ID or Discord tag to us so that we can
          whitelist you, or you won't be able to join.
        </p>
      </Notice>
      {contents}
    </>
  );
};

export default Signup;
