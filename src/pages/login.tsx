import React from "react";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import { FormikHelpers, Formik } from "formik";

import { login, AuthContext } from "../auth";
import log from "../log";
import AuthMessages from "../auth/messages";
import LoginForm, {
  LoginFormValues,
  initialValues,
  schema,
} from "../forms/login";
import { HTTPError } from "../request";
import Notice from "../components/Notice";
import Hue from "../hue";

export default function Login() {
  const router = useRouter();
  const authState = React.useContext(AuthContext);

  function getDestination(): string {
    const manualDestination = router.query.redirect_to as string | null;

    if (manualDestination != null && !manualDestination.startsWith("/")) {
      // prevent being redirected out of origin
      return "/";
    }

    return manualDestination ?? "/";
  }

  if (authState.authed) {
    if (process.browser) {
      router.replace(getDestination());
    }
    return <p>Redirecting you...</p>;
  }

  async function handleSubmit(
    values: LoginFormValues,
    actions: FormikHelpers<LoginFormValues>
  ): Promise<void> {
    try {
      await login(values.username, values.password, {
        save: values.rememberMe,
      });
    } catch (err) {
      log("failed to login:", err);

      let status: string | React.ReactNode;
      if (err instanceof TypeError) {
        status = "Failed to login. Are you connected to the internet?";
      } else if (err instanceof HTTPError) {
        status =
          (err.serverMessage ? AuthMessages[err.serverMessage] : null) ||
          err.serverMessage;
      }

      actions.setStatus(status);
      return;
    } finally {
      actions.setSubmitting(false);
    }

    if (authState.actions != null) {
      await authState.actions.refreshAuthState();
    } else {
      // Manually reload the app if we don't have any actions for some reason.
      window.location.href = getDestination();
    }

    // Go to where we need to be, now that we're logged in.
    router.replace(getDestination());
  }

  const isBeingRedirectedElsewhere = getDestination() !== "/";

  return (
    <>
      <h1>Login</h1>

      {isBeingRedirectedElsewhere && (
        <Notice hue={Hue.Yellow}>
          Before you can access that page, you have to log in first.
        </Notice>
      )}

      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        component={LoginForm}
        validationSchema={schema}
      />

      <p>
        <Link href="/forgot">
          <a>Forgot password or username?</a>
        </Link>
      </p>
    </>
  );
}
