import React from "react";
import styled from "styled-components";
import { NextPageContext } from "next";
import {
  ListboxInput,
  ListboxButton,
  ListboxList,
  ListboxPopover,
  ListboxOption,
} from "@reach/listbox";

import { Profile } from "../api/profile";
import { Domain } from "../api/domain";
import withEnforcedLogin from "../hoc/withEnforcedLogin";
import request, { HTTPError } from "../request";
import Notice from "../components/Notice";
import FormGroup from "../forms/components/FormGroup";
import Hue from "../hue";
import Note from "../components/Note";
import UsageMeter from "../components/UsageMeter";

const UsageMeters = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 1rem;
`;

function mebibytes(bytes: number): number {
  // 1048576 = 100 MiB
  return parseFloat((bytes / 1048576).toFixed(1));
}

type Props = {
  profile: Profile | null;
  domains: Domain[] | null;
  error: string | null;
};

function DomainChooser({
  id,
  label,
  nullOption,
  domains,
  value,
  onChange,
}: {
  id: string;
  label: string;
  value: string;
  defaultValue: string;
  nullOption?: string;
  domains: Domain[];
  onChange: (value: string) => void;
}) {
  const selectedDomain = domains.find(domain => domain.id.toString() === value);
  const buttonText = selectedDomain?.domain ?? nullOption ?? domains[0].domain;

  return (
    <FormGroup>
      <label htmlFor={id}>{label}</label>
      <ListboxInput id={id} onChange={onChange}>
        <ListboxButton arrow="▼">{buttonText}</ListboxButton>
        <ListboxPopover>
          <ListboxList>
            {nullOption && (
              <ListboxOption value="null">{nullOption}</ListboxOption>
            )}
            {domains.map(domain => (
              <ListboxOption key={domain.id} value={domain.id.toString()}>
                {domain.domain}
              </ListboxOption>
            ))}
          </ListboxList>
        </ListboxPopover>
      </ListboxInput>
    </FormGroup>
  );
}

function Account({ profile, domains, error }: Props) {
  const [domain, setDomain] = React.useState(
    profile?.domain.toString() ?? "..."
  );
  const [shortenDomain, setShortenDomain] = React.useState(
    profile?.shorten_domain?.toString() ?? "null"
  );
  const [editError, setEditError] = React.useState<string | null>(null);

  async function patch(payload: Record<string, any>) {
    try {
      await request("/api/v3/profile", {
        method: "PATCH",
        json: payload,
      });

      setEditError(null);
    } catch (error) {
      setEditError(
        error instanceof HTTPError ? error.message : error.toString()
      );
    }
  }

  if (error != null) {
    return (
      <Notice hue={Hue.Red}>
        <strong>Failed to load your information:</strong> {error.toString()}
      </Notice>
    );
  }

  if (profile == null || domains == null) {
    return (
      <>
        <h2>Account</h2>
        <p>Loading your account information, sit tight...!</p>
      </>
    );
  }

  return (
    <>
      <h2>Account</h2>
      <p>Hey there, {profile.name}!</p>

      <h3>Usage</h3>

      <UsageMeters>
        <UsageMeter
          id="file-usage-meter"
          value={profile.limits.file_byte_used}
          max={profile.limits.file_byte_limit}
          formatter={mebibytes}
          unit="MiB"
          label="Images"
        />
        <UsageMeter
          id="shorten-usage-meter"
          value={profile.limits.shorten_used}
          max={profile.limits.shorten_limit}
          label="Shortens"
        ></UsageMeter>
      </UsageMeters>

      <Note>
        You can only upload {mebibytes(profile.limits.file_byte_limit)} MiB in a
        7 day period. Additionally, you can only upload 100 MiB at a time
        regardless of your limits.
      </Note>

      {editError && (
        <Notice hue={Hue.Red}>
          <strong>Failed to make a change:</strong> {editError}
        </Notice>
      )}

      <h3>Domains</h3>

      <p>
        Choose which domains are used by default when uploading images and
        shortening links:
      </p>

      <DomainChooser
        id="upload-domain-chooser"
        defaultValue={profile.domain.toString()}
        domains={domains}
        label="Upload Domain"
        value={domain}
        onChange={value => {
          if (domain === value) return;
          setDomain(value);
          patch({ domain: parseInt(value, 10) });
        }}
      />

      <DomainChooser
        id="shorten-domain-chooser"
        defaultValue={profile.shorten_domain?.toString() ?? "null"}
        domains={domains}
        label="Shorten Domain"
        nullOption="(use upload domain)"
        value={shortenDomain}
        onChange={value => {
          if (shortenDomain === value) return;
          setShortenDomain(value);
          patch({
            shorten_domain: value === "null" ? null : parseInt(value, 10),
          });
        }}
      />

      <h3>Settings</h3>
    </>
  );
}

Account.getInitialProps = async function (
  ctx: NextPageContext
): Promise<Props> {
  try {
    const profile = await request<Profile>("/api/profile", { ctx });
    const domains = await request<{ domains: Domain[] }>("/api/domains");
    return { profile, domains: domains.domains, error: null };
  } catch (error) {
    return { profile: null, domains: null, error: error.toString() };
  }
};

export default withEnforcedLogin(Account, "/account");
