declare module "nanoid" {
  export default function generate(size: number = 21): string;
}

declare module "nanoevents" {
  declare function unbind(): void;

  export default class NanoEvents<E extends Record<string, function>> {
    public emit<V extends keyof E>(
      name: V,
      ...params: Parameters<E[V]>
    ): typeof unbind;
    public on<V extends keyof E>(name: V, callback: E[V]): void;
  }
}
