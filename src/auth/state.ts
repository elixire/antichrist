import { NextPageContext } from "next";

import request, { RequestOptions } from "../request";
import { getTokenFromCookies } from "./cookie";
import { debugLog } from "../log";

export interface AuthStateActions {
  refreshAuthState: () => Promise<void>;
}

/**
 * The baseline auth state.
 *
 * Actions will always be provided by <AntichristApp/> in order to let any child
 * component trigger a refresh. Until they are injected however, `actions` will
 * be `null`.
 */
interface BaseAuthState {
  authed: boolean;
  actions: AuthStateActions | null;
}

export interface GuestAuthState extends BaseAuthState {
  authed: false;
}

export interface LoggedInAuthState extends BaseAuthState {
  authed: true;

  /**
   * The profile object that is stored on the client.
   * This is `null` when server rendering and a token was provided via cookie.
   */
  profile: Record<string, any>;

  /** The token. */
  token: string;
}

export type AuthState = GuestAuthState | LoggedInAuthState;

export type ComputeAuthStateOptions = {
  /** The Next.js page context, if applicable. */
  serverContext: NextPageContext | null;

  /** Whether to output debug messages or not. */
  quiet: boolean;
};

/**
 * Computes the current `AuthState` from `window.localStorage` or
 * `window.sessionStorage`.
 *
 * @param actions - The actions object (can be null).
 */
export async function computeAuthState(
  actions: AuthStateActions | null,
  options: ComputeAuthStateOptions
): Promise<AuthState> {
  const log = (...args: any[]) => {
    if (!options.quiet) debugLog(...args);
  };

  const cookies = options.serverContext
    ? options.serverContext.req?.headers?.cookie
    : document.cookie;

  if (!cookies) {
    log("no cookie found \u2234 not authed");
    return { actions, authed: false };
  }

  const token = getTokenFromCookies(cookies);

  if (!token) {
    log("no token found \u2234 not authed");
    return { actions, authed: false };
  }

  const requestOptions: RequestOptions = options.serverContext
    ? { ctx: options.serverContext }
    : {};
  const profile = await request<object>("/api/v3/profile", requestOptions);

  log("grabbed profile \u2234 authed");
  return { actions, authed: true, profile, token };
}
