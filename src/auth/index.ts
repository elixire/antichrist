import React from "react";

import log from "../log";
import request from "../request";
import { AuthState } from "./state";
import { saveTokenToCookie, deleteTokenCookie } from "./cookie";

export { COOKIE_NAME_TOKEN } from "./keys";

export const AuthContext = React.createContext<AuthState>({
  authed: false,
  actions: null,
});

/**
 * An interface for {@link login} options.
 */
interface LoginOptions {
  /**
   * If `true`, save the user token into a persistent cookie and save the user
   * profile into `window.localStorage` instead of `window.sessionStorage`.
   */
  save: boolean;
}

/**
 * Authenticate with elixire.
 *
 * `/api/login` is `POST`ed to in other to validate the provided credentials and
 * obtain a token.
 *
 * @param username - The username of the user to login as.
 * @param password - The password of the user to login as.
 * @param options - The {@link LoginOptions | login options}.
 *
 * @returns The token.
 */
export async function login(
  username: string,
  password: string,
  options: LoginOptions = { save: false }
): Promise<string> {
  log(`logging in (username: ${username})`);

  const { token } = await request<{ token: string }>("/api/auth/login", {
    method: "POST",
    json: { authdata_type: "password", username, authdata: { password } },
  });

  if (options.save) {
    log("*** saving token and profile");
    saveTokenToCookie(token, { maxAge: 60 * 60 * 24 * 365 });
  } else {
    // only remember the token for this session only
    saveTokenToCookie(token);

    // nuke creds before closing tab
    window.addEventListener("beforeunload", () => {
      deleteTokenCookie();
    });
  }

  const obscuredToken = token.slice(0, 10) + "*".repeat(token.length - 10);
  log(`logged in: ${obscuredToken}`);

  return token;
}
