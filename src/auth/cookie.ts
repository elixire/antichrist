import config from "../../config.json";
import { COOKIE_NAME_TOKEN } from "./keys";
import log from "../log";

function getCookieOptions(): string {
  if (process.env.NODE_ENV === "development") {
    // in development, just use the same host
    return `;samesite=strict;domain=${window.location.hostname}`;
  } else {
    // in production, use the configured cookie domain, and only allow it in
    // https
    return `;samesite=strict;domain=${config.cookieDomain};secure`;
  }
}

export function saveTokenToCookie(
  token: string,
  options: { maxAge?: number } = {}
): void {
  log("*** saving token in cookie");

  let cookie = `${COOKIE_NAME_TOKEN}=${token}` + getCookieOptions();

  if (options.maxAge != null) {
    cookie += `;max-age=${options.maxAge}`;
  }

  document.cookie = cookie;
}

export function deleteTokenCookie(): void {
  document.cookie =
    `${COOKIE_NAME_TOKEN}=x` + getCookieOptions() + `;max-age=0`;
}

export function findCookieByName(name: string, cookies: string): string | null {
  const cookiePairs = (cookies || document.cookie)
    .split(";")
    .map(mapping => mapping.trim().split("=") as [string, string]);
  const cookiesMap = new Map<string, string>(cookiePairs);
  return cookiesMap.get(name) || null;
}

/**
 * Gets the elixire token from a string of all cookies.
 *
 * @param cookies The string containing all of the cookies.
 */
export function getTokenFromCookies(cookies: string): string | null {
  return findCookieByName(COOKIE_NAME_TOKEN, cookies);
}
