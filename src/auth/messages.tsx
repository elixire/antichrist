import React from "react";

const AuthMessages: Record<string, React.ReactNode> = {
  "User or password invalid": "Invalid username or password",
  "User is deactivated": (
    <>
      <p>
        <strong>Your account is not activated.</strong>
      </p>
      <p>
        If you have just registered, keep in mind that{" "}
        <b>your account must be approved</b> before you can use it.
      </p>
      <p>
        If you have logged in before, your account may have been deactivated by
        a developer. Please contact us through Discord or email.
      </p>
    </>
  ),
};

export default AuthMessages;
