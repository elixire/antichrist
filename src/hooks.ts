import React from "react";

interface FormReturnValue<F> {
  handleChange(event: React.ChangeEvent): void;
  values: F;
}

export function useForm<F extends { [key: string]: number | string | boolean }>(
  initialState: F
): FormReturnValue<F> {
  const [values, setValues] = React.useState<F>(initialState);

  function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    event.persist();
    const value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;
    setValues(values => ({
      ...values,
      [event.target.name]: value,
    }));
  }

  return {
    handleChange,
    values,
  };
}

export function useEvent<E extends keyof DocumentEventMap>(
  source: EventTarget,
  type: E,
  listener: (event: Event) => void
): void {
  React.useEffect(() => {
    source.addEventListener(type, listener);
    return () => source.removeEventListener(type, listener);
  });
}
