import React from "react";
import { NextPage } from "next";
import Router from "next/router";

import { AuthContext } from "../auth";

export default function withEnforcedLogin<P>(
  Component: NextPage<P>,
  redirectTo: string
) {
  function WrappedComponent(props: P) {
    const authContext = React.useContext(AuthContext);

    if (!authContext.authed) {
      if (process.browser) {
        Router.replace(`/login?redirect_to=${redirectTo}`);
        return null;
      } else {
        return null;
      }
    }

    return <Component {...props} />;
  }

  if (Component.getInitialProps != null) {
    WrappedComponent.getInitialProps = Component.getInitialProps;
  }

  return WrappedComponent;
}
