function woodchop(
  options: { format: string; color: string; allowServer: boolean },
  chunks: any[]
): void {
  if (!options.allowServer && !process.browser) {
    return;
  }

  if (options.allowServer && !process.browser) {
    console.log(options.format, ...chunks);
    return;
  }

  console.log(
    `%c${options.format}%c`,
    `color: ${options.color}; font-weight: bold`,
    "color: inherit; font-weight: inherit",
    ...chunks
  );
}

export default function log(...chunks: any[]): void {
  // *megaupload voice*
  woodchop(
    { format: "[elixi.re]", color: "orange", allowServer: false },
    chunks
  );
}

export function debugLog(...chunks: any[]): void {
  if (process.env.NODE_ENV !== "development") {
    return;
  }

  woodchop(
    {
      format: "[\u{1f6a7}\u{1f6a7}\u{1f6a7}]",
      color: "yellow",
      allowServer: true,
    },
    chunks
  );
}
