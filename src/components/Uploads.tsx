import React from "react";
import styled from "styled-components";

import UploadComponent from "./Upload";
import { Upload } from "../api/upload";
import Note from "./Note";
import { NavigatorContext } from "../context";

const StyledUploads = styled.div`
  margin: 1rem 0;
`;

type Props = {
  uploads: Upload[];
};

const Uploads: React.FC<Props> = ({ uploads }): React.ReactElement => {
  const { userAgent } = React.useContext(NavigatorContext);
  const isMac = userAgent!.includes("Macintosh");

  const nodes = uploads.map(upload => (
    <UploadComponent upload={upload} key={upload.request.id} />
  ));

  return (
    <>
      <StyledUploads>{nodes}</StyledUploads>
      {uploads.length !== 0 ? (
        <Note>
          Click on a link to copy it. You can open it instead by holding down{" "}
          <kbd>{isMac ? "shift" : "SHIFT"}</kbd> or{" "}
          <kbd>{isMac ? "⌘" : "CTRL"}</kbd>.
        </Note>
      ) : null}
    </>
  );
};

export default Uploads;
