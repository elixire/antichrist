import styled from "styled-components";

const Label = styled.label`
  display: block;
  font-size: 0.75em;
  text-transform: uppercase;
  margin-bottom: 0.5rem;
`;

export default Label;
