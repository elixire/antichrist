import React from "react";
import Link from "next/link";
import styled from "styled-components";

import { instanceName } from "../../config.json";
import { AuthContext } from "../auth";
import { AuthState } from "../auth/state";

const StyledHeader = styled.header`
  display: flex;
  align-items: center;
`;

const HeaderLinks = styled.nav`
  display: flex;
  padding-left: 1rem;

  a:not(:last-child) {
    margin-right: 1rem;
  }
`;

const RightHeaderLinks = styled(HeaderLinks)`
  margin-left: auto;
`;

const InstanceLink = styled.a`
  color: inherit;
  text-decoration: none;
  font-size: 2rem;

  :hover {
    text-decoration: underline;
  }
`;

function renderRightLinks(authState: AuthState): React.ReactElement {
  return authState.authed ? (
    <RightHeaderLinks>
      <Link href="/account">
        <a title={authState.profile.id}>{authState.profile.name}</a>
      </Link>
      <Link href="/logout">
        <a>Logout</a>
      </Link>
    </RightHeaderLinks>
  ) : (
    <RightHeaderLinks>
      <Link href="/login">
        <a>Login</a>
      </Link>
      <Link href="/signup">
        <a>Signup</a>
      </Link>
    </RightHeaderLinks>
  );
}

const Header: React.FC = (): React.ReactElement => {
  const authState = React.useContext(AuthContext);

  // suppress hydration warnings -- the server will not render the right-aligned
  // links (ones that are specific to the current authentication state). the
  // client handles that. but react will complain that the ssr content and the
  // client content are different, so we have to suppress it.
  return (
    <StyledHeader suppressHydrationWarning>
      <Link href="/" passHref>
        <InstanceLink>{instanceName}</InstanceLink>
      </Link>

      <HeaderLinks>
        <Link href="/about">
          <a>About</a>
        </Link>
        <Link href="/about">
          <a>FAQ</a>
        </Link>
        <Link href="/privacy">
          <a>Privacy</a>
        </Link>
      </HeaderLinks>

      {renderRightLinks(authState)}
    </StyledHeader>
  );
};

export default Header;
