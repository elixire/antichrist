import React from "react";
import styled, { keyframes } from "styled-components";
import { useSpring } from "react-spring";
import {
  faSyncAlt,
  faExclamationCircle,
  faCheckCircle,
  faStopCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon as FA } from "@fortawesome/react-fontawesome";

import {
  Upload as UploadObject,
  UploadState,
  UploadResponse,
} from "../../api/upload";
import { AnimatedProgress } from "../Progress";
import { transformErrorMessage } from "./messages";

const StyledUpload = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  margin: 1rem 0;
  padding: 1rem;
  background: hsl(0, 0%, 15%);
  border-radius: 0.25rem;

  svg {
    margin-right: 1rem;
  }
`;

const Name = styled.div`
  font-size: 1.5rem;
  font-weight: bold;
  margin: 0 0 0.5rem 0;
`;

const UploadInfo = styled.div`
  width: 100%;
`;

const UploadedLink = styled.a`
  line-height: 1;
`;

const UploadFlashAnimation = keyframes`
  from {
    opacity: 0.75;
  }

  to {
    opacity: 0;
  }
`;

const UploadFlash = styled.span`
  margin-left: 0.5em;
  opacity: 0.75;
  font-size: 0.75em;
  animation: 2s ease-out 1s 1 forwards ${UploadFlashAnimation};
`;

const uploadStateIcons = {
  [UploadState.INACTIVE]: faSyncAlt,
  [UploadState.UPLOADING]: faSyncAlt,
  [UploadState.SUCCEEDED]: faCheckCircle,
  [UploadState.ERRORED]: faExclamationCircle,
  [UploadState.ABORTED]: faStopCircle,
};

type Props = {
  upload: UploadObject;
};

function computeErrorStatus(upload: UploadObject): React.ReactNode {
  if (upload.request?.request?.status === 413) {
    return <span>File is too big.</span>;
  } else if (upload.error && upload.error.message) {
    return <span>{transformErrorMessage(upload.error.message)}</span>;
  }

  return <span>Upload failed.</span>;
}

function renderStatus(upload: UploadObject): React.ReactNode {
  const [showCopyFlash, setShowCopyFlash] = React.useState(false);
  const animatedProps = useSpring({
    to: { value: upload.progress * 100 },
  });

  async function copy(resp: UploadResponse) {
    try {
      await navigator.clipboard.writeText(resp.url);
      if (!showCopyFlash) {
        setShowCopyFlash(true);
        setTimeout(() => setShowCopyFlash(false), 3000);
      }
    } catch (error) {
      alert(
        `Failed to copy link (${error}). Maybe you need to give the website permission?`
      );
    }
  }

  switch (upload.state) {
    case UploadState.INACTIVE:
    case UploadState.UPLOADING:
      return <AnimatedProgress max={100} {...animatedProps} />;
    case UploadState.SUCCEEDED:
      const resp = upload.response! as UploadResponse;

      return (
        <>
          <UploadedLink
            target="_blank"
            rel="noreferrer"
            onClick={(event: React.MouseEvent) => {
              if (!event.metaKey && !event.shiftKey && !event.altKey) {
                event.preventDefault();
                copy(resp);
              }
            }}
            href={resp.url}
          >
            {resp.url}
          </UploadedLink>
          {showCopyFlash ? <UploadFlash>copied!</UploadFlash> : null}
        </>
      );
    case UploadState.ERRORED:
      return computeErrorStatus(upload);
    case UploadState.ABORTED:
      return <span>Upload aborted.</span>;
  }
}

const Upload: React.FC<Props> = ({ upload }): React.ReactElement => {
  return (
    <StyledUpload>
      <FA
        size="2x"
        icon={uploadStateIcons[upload.state]}
        fixedWidth
        spin={[UploadState.UPLOADING, UploadState.INACTIVE].includes(
          upload.state
        )}
      />
      <UploadInfo>
        <Name>{upload.request.file.name}</Name>
        {renderStatus(upload)}
      </UploadInfo>
    </StyledUpload>
  );
};

export default Upload;
