export const MESSAGES = new Map<RegExp, string>([
  [/Bad mime type: '(.+)'/, 'You can\'t upload "$1" files.'],
]);

export function transformErrorMessage(message: string): string {
  let transformed = message;

  MESSAGES.forEach((to, from) => {
    transformed = transformed.replace(from, to);
  });

  if (transformed === message) {
    return `Error: ${message}`;
  }

  return transformed;
}
