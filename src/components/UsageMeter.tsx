import React from "react";
import styled from "styled-components";

import Label from "./Label";

const Meter = styled.div<{ percentage: number }>`
  background-color: hsla(0, 100%, 100%, 5%);
  background-image: linear-gradient(
    to right,
    var(--primary) ${props => props.percentage * 100}%,
    transparent ${props => props.percentage * 100}%
  );
  border-radius: 0.25rem;
  padding: 1rem;
  font-size: 1.5em;
  text-align: right;
  line-height: 1;
`;

const Big = styled.span`
  font-size: 1.75em;
`;

type Props = {
  id: string;
  value: number;
  max: number;
  formatter: (value: number) => number;
  unit: string;
  label: string;
};

export default function UsageMeter(props: Props): React.ReactElement {
  const formattedValue = props.formatter(props.value);
  const formattedMax = props.formatter(props.max);

  function withUnit(value: number): string {
    return props.unit !== "" ? `${value} ${props.unit}` : value.toString();
  }

  const labelId = `${props.id}-label`;
  const meterLabel =
    `${withUnit(formattedValue)} out of ` + `${withUnit(formattedMax)} used`;

  return (
    <div>
      <Label id={labelId}>{props.label}</Label>
      <Meter
        percentage={props.value / props.max}
        aria-describedby={labelId}
        aria-label={meterLabel}
      >
        <Big>{formattedValue}</Big> / {formattedMax} {props.unit}
      </Meter>
    </div>
  );
}

UsageMeter.defaultProps = {
  max: 1,
  formatter: (value: number) => value,
  unit: "",
  label: "Meter",
};
