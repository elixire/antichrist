import React from "react";
import Head from "next/head";

import { instanceName } from "../../config.json";
import Notice from "./Notice";
import Hue from "../hue";
import Header from "./Header";

type Props = {
  children: React.ReactNode;
};

const Layout: React.FC<Props> = ({ children }): React.ReactElement => {
  return (
    <div id="main-layout">
      <Head>
        <title key="title">{instanceName}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.svg" />
        <link rel="icon" href="/favicon.png" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
      </Head>
      <Header />
      <main>
        <noscript>
          <Notice hue={Hue.Blue}>
            <strong>Heads up:</strong> you should enable JavaScript, or else
            this website won't be very useful to you.
          </Notice>
        </noscript>
        {children}
      </main>
    </div>
  );
};

export default Layout;
