import styled from "styled-components";
import { animated } from "react-spring";

const Progress = styled.progress`
  display: block;
  width: 100%;
  border: none;
  border-radius: 0.25rem;
  background-color: hsl(0, 0%, 20%);

  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;

  ::-moz-progress-bar {
    border-radius: 0.25rem;
    background-color: var(--primary);
  }

  ::-webkit-progress-bar {
    height: 1rem;
    border-radius: 0.25rem;
    background-color: hsl(0, 0%, 20%);
  }
  ::-webkit-progress-value {
    border-radius: 0.25rem;
    background-color: var(--primary);
  }
`;

export default Progress;

export const AnimatedProgress = animated(Progress);
