import React from "react";
import styled, { css, keyframes } from "styled-components";
import { FontAwesomeIcon as FA } from "@fortawesome/react-fontawesome";
import { faFileUpload } from "@fortawesome/free-solid-svg-icons";
import VisuallyHidden from "@reach/visually-hidden";

import { AuthContext } from "../auth";
import request from "../request";
import { LoggedInAuthState } from "../auth/state";
import { useEvent } from "../hooks";
import {
  UploadRequest,
  Upload,
  UploadState,
  UploadResponse,
} from "../api/upload";
import Uploads from "./Uploads";
import { APIErrorResponse } from "../api";

const dropAnimation = keyframes`
  50% {
    transform: scale(1.125);
  }
`;

const StyledUploader = styled.div<{ fullscreen?: boolean; focused?: boolean }>`
  color: hsl(0, 0%, 80%);
  border: dashed 0.5rem;
  border-color: ${props =>
    props.focused ? css`var(--primary)` : css`hsl(0, 0%, 50%)`};
  border-radius: 0.5rem;
  padding: 3rem;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column nowrap;
  font-size: 2rem;
  margin: 1rem 0;
  cursor: pointer;
  user-select: none;
  z-index: 700;

  transition: border-color 0.1s;

  ${props =>
    props.fullscreen &&
    css`
      margin: 0;
      position: absolute;
      top: 2rem;
      left: 2rem;
      width: calc(100% - 4rem);
      height: calc(100% - 4rem);

      svg {
        animation: 1.5s ease-in-out infinite ${dropAnimation};
      }
    `}

  svg {
    margin: 0 0 2rem 0;
  }

  p {
    margin: 0;
  }
`;

const Dropzone = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  z-index: 1000;
`;

const Dim = styled(Dropzone)`
  z-index: 500;
  background: rgba(0, 0, 0, 70%);
`;

function useUploads(
  { asAdmin }: { asAdmin: boolean } = { asAdmin: false }
): { uploads: Upload[]; addUpload: (request: UploadRequest) => void } {
  const [uploads, setUploads] = React.useState<Upload[]>([]);

  function editUpload(
    request: UploadRequest,
    properties: Partial<Upload>
  ): void {
    setUploads(uploads =>
      uploads.map(upload =>
        upload.request.id === request.id ? { ...upload, ...properties } : upload
      )
    );
  }

  function setUploadState(request: UploadRequest, state: UploadState): void {
    editUpload(request, { state });
  }

  function addUpload(request: UploadRequest): void {
    setUploads(uploads =>
      uploads.concat([
        {
          progress: 0,
          response: null,
          error: null,
          state: UploadState.INACTIVE,
          request,
        },
      ])
    );

    request.on("progress", percentage =>
      editUpload(request, { progress: percentage })
    );

    request.on("succeed", response =>
      editUpload(request, {
        response: response as UploadResponse,
        state: UploadState.SUCCEEDED,
      })
    );

    request.on("abort", () => setUploadState(request, UploadState.ABORTED));
    request.on("start", () => setUploadState(request, UploadState.UPLOADING));
    request.on("error", (response: object | null) =>
      editUpload(request, {
        state: UploadState.ERRORED,
        ...(response != null && { error: response as APIErrorResponse }),
      })
    );

    request.send({ asAdmin });
  }

  return { uploads, addUpload };
}

export default function Uploader(): React.ReactElement {
  const [allowedMimes, setAllowedMimes] = React.useState<string[]>([]);
  const [focused, setFocused] = React.useState<boolean>(false);
  const [dropzoneActive, setDropzoneActive] = React.useState(false);
  const authState = React.useContext(AuthContext) as LoggedInAuthState;

  const isAdmin = authState.profile != null && authState.profile.admin;
  const targetLabel = isAdmin ? "files" : "images";

  const { uploads, addUpload } = useUploads({ asAdmin: isAdmin });

  const extraFileInputAttributes: React.PropsWithoutRef<
    JSX.IntrinsicElements["input"]
  > =
    // Admins can upload any type of file.
    allowedMimes.length === 0 || isAdmin
      ? {}
      : { accept: allowedMimes.join(", ") };

  React.useEffect(() => {
    if (isAdmin) {
      return;
    }

    request<{ accepted_mimes: string[] }>("/api/hello").then(data => {
      setAllowedMimes(data.accepted_mimes);
    });
  }, []);

  function addFiles(fileList: FileList): void {
    const files = Array.from(fileList);
    if (files.length === 0) {
      return;
    }

    for (const file of files) {
      if (file.name === "") {
        // probably a folder...
        continue;
      }

      addUpload(new UploadRequest(file));
    }
  }

  if (process.browser) {
    useEvent(document, "dragenter", () => {
      setDropzoneActive(true);
    });

    useEvent(document, "paste", (event: Event) => {
      let clipboardData = (event as ClipboardEvent).clipboardData;
      if (clipboardData != null) {
        addFiles(clipboardData.files);
      }
    });
  }

  function handleDrop(event: React.DragEvent<HTMLDivElement>): void {
    event.preventDefault();
    addFiles(event.dataTransfer.files);
    setDropzoneActive(false);
  }

  function handleDragLeave(): void {
    setDropzoneActive(false);
  }

  function handleDragOver(event: React.DragEvent<HTMLDivElement>): void {
    event.preventDefault();
    event.dataTransfer.dropEffect = "copy";
  }

  function handleFileChange(event: React.ChangeEvent<HTMLInputElement>): void {
    if (event.target.files != null) {
      addFiles(event.target.files);
    }
  }

  return (
    <>
      {dropzoneActive && (
        <>
          <Dim />
          <Dropzone
            onDragOver={handleDragOver}
            onDragLeave={handleDragLeave}
            onDrop={handleDrop}
            onDragEnd={handleDragLeave}
          />
        </>
      )}
      <form>
        <label htmlFor="file" aria-hidden>
          <StyledUploader fullscreen={dropzoneActive} focused={focused}>
            <FA icon={faFileUpload} size="3x" />
            {dropzoneActive ? (
              <p>Drop to upload</p>
            ) : (
              <p suppressHydrationWarning>
                Drop, paste, or select {targetLabel}
              </p>
            )}
          </StyledUploader>
        </label>
        <VisuallyHidden>
          <input
            id="file"
            type="file"
            multiple
            onChange={handleFileChange}
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
            {...extraFileInputAttributes}
          />
        </VisuallyHidden>
      </form>
      <Uploads uploads={uploads} />
    </>
  );
}
