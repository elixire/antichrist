import React from "react";
import styled from "styled-components";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon as FA } from "@fortawesome/react-fontawesome";

const StyledNote = styled.div`
  color: hsla(0, 100%, 100%, 50%);
  display: flex;
  flex-flow: row nowrap;
  margin: 1rem 0;
`;

const Icon = styled.div`
  margin-right: 0.5rem;
`;

const NoteParagraph = styled.p`
  /* <StyledNote/> already has a margin */
  margin: 0;
`;

export default function Note({ children }: { children: React.ReactNode }) {
  return (
    <StyledNote>
      <Icon>
        <FA icon={faInfoCircle} />
      </Icon>
      <NoteParagraph>{children}</NoteParagraph>
    </StyledNote>
  );
}
