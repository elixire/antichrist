import styled from "styled-components";

import Hue, { hueBorder, hueBackground } from "../hue";

const Notice = styled.div<{ hue?: Hue }>`
  padding: 1rem;
  margin: 1rem 0;
  border-radius: 0.25rem;

  ${props =>
    props.hue != null
      ? `
        ${hueBorder(props.hue)}
        ${hueBackground(props.hue)}
      `
      : `
        border: solid 1px hsl(0, 0%, 25%);
        background-color: hsl(0, 0%, 15%);
      `}

  > *:first-child {
    margin-top: 0;
  }

  > *:last-child {
    margin-bottom: 0;
  }
`;

export default Notice;
