/**
 * Fetch a value from `window.localStorage` or `window.sessionStorage`.
 * Returns `null` when ran on the server.
 */
export function fetchFromStorage(key: string): string | null {
  if (!process.browser) return null;
  const value = window.localStorage.getItem(key);
  return value == null ? window.sessionStorage.getItem(key) : value;
}
