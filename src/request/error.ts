export class RequestError extends Error {}

export class HTTPError extends RequestError {
  public resp: Response;

  /** The error message provided by the server in the JSON response. */
  public serverMessage: string | null;

  public constructor(resp: Response, message: string | null) {
    let errorMessage = `HTTP ${resp.status} ${resp.statusText}`;
    if (errorMessage != null) {
      errorMessage += ` (${message})`;
    }
    super(errorMessage);

    this.resp = resp;
    this.serverMessage = message;
  }
}
