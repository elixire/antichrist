import { NextPageContext } from "next";

import { version as ANTICHRIST_VERSION } from "../../package.json";
import config from "../../config.json";
import { getTokenFromCookies } from "../auth/cookie";
import { RequestError, HTTPError } from "./error";

export { RequestError, HTTPError };

export function computeRequestHeaders(
  cookies: string | null
): Record<string, string> {
  const requesterType = process.browser ? "browser" : "server renderer";
  const userAgent = `antichrist/${ANTICHRIST_VERSION} (${requesterType})`;

  // in the browser, we can't specify a custom user-agent, so let's use a custom
  // header instead
  let base: Record<string, string> = process.browser
    ? { "X-Antichrist-Version": userAgent }
    : { "User-Agent": userAgent };

  // required header
  base["X-Elixire-Client"] = userAgent;

  if (cookies != null) {
    let token;
    if ((token = getTokenFromCookies(cookies)) != null) {
      base.Authorization = token;
    }
  }

  return base;
}

/** Returns the server URL to be used in requests. */
export function serverUrl(): string {
  const serverUrl = process.browser ? config.externalUrl : config.internalUrl;
  // remove a trailing slash if present
  return serverUrl.endsWith("/") ? serverUrl.slice(0, -1) : serverUrl;
}

export interface RequestOptions extends RequestInit {
  json?: Record<string, any>;
  cookies?: string | null;
  ctx?: NextPageContext;
  headers?: Record<string, string>;
}

export default async function request<D>(
  input: RequestInfo,
  options: RequestOptions = {}
): Promise<D> {
  // by default, use no cookies (unauthenticated request)
  let defaultCookies = null;

  if (process.browser) {
    // use cookies from the browser
    defaultCookies = document.cookie;
  } else if (options.ctx != null) {
    // use cookies from the server request
    defaultCookies = options.ctx.req?.headers?.cookie;
  }

  let requestOptions: RequestInit = {
    headers: {
      ...computeRequestHeaders(options.cookies || defaultCookies || null),
      ...(options.headers || {}),
    } as Record<string, string>,
  };

  const addHeader = (header: string, value: string) => {
    (requestOptions.headers as Record<string, string>)[header] = value;
  };

  if ("ctx" in options) {
    // If `ctx` is in `options`, then we are the server renderer making a
    // request to the elixire backend.
    //
    // When that happens, the backend will only ever see _our_ address, which
    // means that ratelimits are effectively broken as everyone would be put
    // into the same bucket.
    //
    // To remedy this, let's give the backend the address of the client which
    // kicked off this request, so it can use that instead.
    //
    // If the X-Forwarded-For header is present on the client request (and the
    // config lets us), use that header. This is useful in circumstances where
    // we are serving behind a reverse proxy, where we want the IP of the
    // client behind the proxy and not the proxy itself.
    let remoteAddress: string | null | undefined;
    if (config.respectXForwardedFor ?? false) {
      // TODO: Don't `.toString()` just to avoid string[].
      remoteAddress = options.ctx?.req?.headers["x-forwarded-for"]?.toString();
    }
    remoteAddress ??= options.ctx?.req?.socket?.remoteAddress;

    if (remoteAddress != null) {
      addHeader("X-Forwarded-For", remoteAddress);
    }

    delete options.ctx;
  }

  if ("cookies" in options) {
    delete options.cookies;
  }

  if ("json" in options) {
    addHeader("Content-Type", "application/json");
    requestOptions.body = JSON.stringify(options.json);
    delete options.json;
  }

  // Merge provided options.
  requestOptions = { ...requestOptions, ...options };

  if (
    typeof input === "string" &&
    !input.startsWith("/") &&
    process.env.NODE_ENV !== "production"
  ) {
    console.warn(
      "The provided endpoint of fetch() calls should have a leading slash."
    );
    input = "/" + input;
  }

  const resp = await fetch(
    typeof input === "string" ? serverUrl() + input : input,
    requestOptions
  );

  // json data should always be provided, even during an error (except for http
  // 500s, apparently).
  //
  // we try to use this data in the `HTTPError` if possible.
  let jsonData: Record<string, any> = {};
  try {
    jsonData = await resp.json();
  } catch (err) {
    jsonData = {};
    throw new RequestError(`Failed to parse JSON response: ${err}`);
  }

  if (!resp.ok || jsonData.error) {
    throw new HTTPError(resp, jsonData.message);
  }

  return jsonData as D;
}
