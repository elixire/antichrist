import React from "react";
import Link from "next/link";
import { FormikProps, Form } from "formik";
import { object, ref, string, boolean } from "yup";

import { username, discordTag, password } from "./schema";
import Note from "../components/Note";
import FormStatusError from "./components/FormStatusError";
import FormGroup from "./components/FormGroup";
import EnhancedField from "./components/EnhancedField";
import EnhancedCheckbox from "./components/EnhancedCheckbox";
import SubmitButton from "./components/SubmitButton";

export interface SignupFormValues {
  username: string;
  email: string;
  discordTag: string;
  password: string;
  passwordConfirm: string;
  agreeToRules: boolean;
}

export const initialValues: SignupFormValues = {
  username: "",
  email: "",
  discordTag: "",
  password: "",
  passwordConfirm: "",
  agreeToRules: false,
};

export const schema = object().shape({
  username: username.required("A username is required"),
  email: string().email("This isn't an email").required("An email is required"),
  discordTag: discordTag.required("A Discord tag is required"),
  password: password.required("A password is required"),
  passwordConfirm: string()
    .oneOf([ref("password")], "Password does not match")
    .required("Confirm your password, please"),
  agreeToRules: boolean().oneOf([true], "You must agree to the rules"),
});

export default function SignupForm(
  props: FormikProps<SignupFormValues>
): React.ReactElement {
  const passwordPlaceholder = "•".repeat(20);

  return (
    <Form>
      <FormStatusError />
      <EnhancedField
        id="signup-username"
        label="Username"
        type="text"
        name="username"
        placeholder="joe"
        autoComplete="off"
      />
      <Note>
        Usernames must be 3 to 20 characters in length; can only consist of
        letters, numbers, or underscores; and cannot start with an underscore.
      </Note>
      <EnhancedField
        id="signup-email"
        label="Email"
        type="email"
        name="email"
        placeholder="joe@ericsson.com"
      />
      <EnhancedField
        id="signup-discordtag"
        label="Discord Tag"
        type="text"
        name="discordTag"
        placeholder="joe#7926"
      />
      <EnhancedField
        id="signup-password"
        label="Password"
        type="password"
        name="password"
        placeholder={passwordPlaceholder}
      />
      <Note>Your password must be 8 to 72 characters in length.</Note>
      <EnhancedField
        id="signup-confirmpassword"
        label="Confirm Password"
        type="password"
        name="passwordConfirm"
        placeholder={passwordPlaceholder}
      />
      <EnhancedCheckbox id="signup-agree" name="agreeToRules">
        <span>
          I agree to the{" "}
          <Link href="/faq">
            <a>rules</a>
          </Link>{" "}
          and{" "}
          <Link href="/privacy">
            <a>the privacy policy</a>
          </Link>
          .
        </span>
      </EnhancedCheckbox>
      <FormGroup>
        <SubmitButton>Request an Account</SubmitButton>
      </FormGroup>
    </Form>
  );
}
