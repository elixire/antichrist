import React from "react";
import styled from "styled-components";
import { Field, FieldProps } from "formik";

import FormGroup from "./FormGroup";
import EnhancedErrorMessage from "./EnhancedErrorMessage";

export const CheckboxLabel = styled.label`
  display: flex !important;
  align-items: baseline;
`;

type Props = {
  id: string;
  name: string;
} & React.InputHTMLAttributes<HTMLInputElement>;

export default function EnhancedCheckbox(props: Props) {
  return (
    <FormGroup>
      <CheckboxLabel htmlFor={props.id}>
        <Field id={props.id} name={props.name}>
          {(fieldProps: FieldProps) => (
            <input
              disabled={fieldProps.form.isSubmitting}
              type="checkbox"
              {...fieldProps.field}
              checked={fieldProps.field.value}
            />
          )}
        </Field>
        {props.children}
      </CheckboxLabel>
      <EnhancedErrorMessage name={props.name} />
    </FormGroup>
  );
}
