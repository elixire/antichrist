import React from "react";
import { Field, FieldConfig, FieldProps } from "formik";

import EnhancedErrorMessage from "./EnhancedErrorMessage";
import FormGroup from "./FormGroup";

type Props = React.InputHTMLAttributes<HTMLInputElement> &
  FieldConfig & {
    id: string;
    label: string;
  };

const EnhancedField: React.FC<Props> = ({ id, label, ...fieldProps }) => {
  const field = (
    <Field id={id} name={fieldProps.name}>
      {(props: FieldProps) => {
        const className =
          props.form.errors[props.field.name] &&
          props.form.touched[props.field.name]
            ? "form-invalid"
            : "";

        return (
          <input
            disabled={props.form.isSubmitting}
            className={className}
            {...fieldProps}
            {...props.field}
          />
        );
      }}
    </Field>
  );

  return (
    <FormGroup>
      <label htmlFor={id}>{label}</label>
      {field}
      <EnhancedErrorMessage name={fieldProps.name} />
    </FormGroup>
  );
};

export default EnhancedField;
