import React from "react";
import { useFormikContext } from "formik";

import Notice from "../../components/Notice";
import Hue from "../../hue";

type Props = {
  hue?: Hue;
};

export default function FormStatusError(props: Props) {
  const formikContext = useFormikContext();

  if (formikContext.status == null) {
    return null;
  }

  return <Notice hue={props.hue}>{formikContext.status}</Notice>;
}

FormStatusError.defaultProps = {
  hue: Hue.Red,
};
