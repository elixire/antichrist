import { ErrorMessage } from "formik";
import styled from "styled-components";

const EnhancedErrorMessage = styled(ErrorMessage).attrs({
  component: "div",
})`
  margin: 0.5rem 0;
  color: hsl(0, 40%, 40%);
`;

export default EnhancedErrorMessage;
