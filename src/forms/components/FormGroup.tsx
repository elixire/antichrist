import styled from "styled-components";

const FormGroup = styled.div`
  margin: 1rem 0;

  input[type="text"],
  input[type="password"],
  input[type="email"] {
    width: 15em;
  }

  label {
    display: block;
    margin: 0.5rem 0;
  }

  input[type="checkbox"] {
    margin: 0 0.5rem 0 0;
  }
`;

export default FormGroup;
