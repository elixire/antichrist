import React from "react";
import { connect } from "formik";

const SubmitButton = connect(props => {
  return (
    <button
      type="submit"
      disabled={props.formik.isSubmitting || !props.formik.isValid}
    >
      {props.children}
    </button>
  );
});

export default SubmitButton;
