import React from "react";

import FormGroup from "../components/FormGroup";

export function withGroup<P>(Component: React.ComponentType<P>) {
  return function WrappedComponent(props: P) {
    return (
      <FormGroup>
        <Component {...props} />
      </FormGroup>
    );
  };
}
