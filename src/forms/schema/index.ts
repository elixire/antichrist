import { string } from "yup";

export const username = string()
  .min(3, "Username must be at least 3 characters")
  .max(20, "Username cannot be longer than 20 characters")
  .matches(/^[a-zA-Z0-9]\w{2,19}$/, "Username contains invalid characters");

export const password = string()
  .min(8, "Password must be at least 8 characters")
  .max(72, "Password cannot be longer than 72 characters");

export const discordTag = string().matches(
  /[^@#:]{2,32}#\d{4}$/,
  "Invalid Discord tag"
);
