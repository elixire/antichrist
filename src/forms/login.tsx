import React from "react";
import { FormikProps, Form } from "formik";
import { object, boolean } from "yup";

import EnhancedField from "./components/EnhancedField";
import EnhancedCheckbox from "./components/EnhancedCheckbox";
import { username, password } from "./schema";
import Notice from "../components/Notice";
import Hue from "../hue";
import FormGroup from "./components/FormGroup";
import FormStatusError from "./components/FormStatusError";
import SubmitButton from "./components/SubmitButton";

export interface LoginFormValues {
  username: string;
  password: string;
  rememberMe: boolean;
}

export const initialValues: LoginFormValues = {
  username: "",
  password: "",
  rememberMe: true,
};

export const schema = object().shape({
  username: username.required("A username is required"),
  password: password.required("A password is required"),
  rememberMe: boolean(),
});

export default function LoginForm(props: FormikProps<LoginFormValues>) {
  const passwordPlaceholder = "•".repeat(20);

  return (
    <Form>
      <FormStatusError />
      <EnhancedField
        id="login-username"
        label="Username"
        type="text"
        name="username"
        placeholder="joe"
      />
      <EnhancedField
        id="login-password"
        label="Password"
        type="password"
        name="password"
        placeholder={passwordPlaceholder}
        maxLength={100}
      />
      <EnhancedCheckbox id="login-remember-me" name="rememberMe">
        Remember Me
      </EnhancedCheckbox>
      {!props.values.rememberMe ? (
        <Notice hue={Hue.Yellow}>
          You'll be logged out when you close this tab.
        </Notice>
      ) : null}
      <FormGroup>
        <SubmitButton>Login</SubmitButton>
      </FormGroup>
    </Form>
  );
}
