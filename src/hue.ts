enum Hue {
  Red = 0,
  Orange = 30,
  Yellow = 60,
  Green = 140,
  Blue = 210,
  Purple = 300,
}

export function hueBackground(hue: Hue): string {
  return `background: hsla(${hue}, 100%, 30%, 50%);`;
}

export function hueBorder(hue: Hue): string {
  return `border: solid 1px hsl(${hue}, 100%, 30%);`;
}

export default Hue;
